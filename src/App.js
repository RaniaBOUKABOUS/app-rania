import React ,{ Component } from 'react'
import './App.css';
import MaterialTable  from 'material-table';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import XLSX from 'xlsx';
class App extends Component { // tnajem tnedi des fonctions mawjoudin f component
 constructor () {
    super (); //tjib les methode min classe personne
    this.state ={ //lobjet li n7oto fiha state mte3na : variable changable 
      client : []
    };
  }
  componentDidMount(){ // parmis cycle de vie de composent :wa9et yatle3 composent l dom() fi firstrending
    fetch ('http://41.226.165.26:8242/Clients/2/2')
    .then((response)=>{ // then :pomisse mrthode 
      return response.json() //response  traja3halik reponse mte3 les apis sous forme json
    }) 
   .then((result)=>{
    console.log('response',result)
     this.setState({client :result})// result li hiya response ; setSate tnajem tbedalik state mte3ik 
                                        //kanet fer8a walla fiha result
     
   })
    
  }
  
   render(){ //foction mawjouda f component 

    const downloadExel=()=> {
      const newData=this.state.client.map(row=>{
        delete row.tableData
        return row 
      })
      const worKSheet =XLSX.utils.json_to_sheet(newData) //xlsx : extension de fichier excel
      const worKBook =XLSX.utils.book_new()
      XLSX.utils.book_append_sheet(worKBook,worKSheet,"client")
      let buffer=XLSX.write(worKBook,{bookType:"xlsx",type:"buffer"})
      XLSX.write(worKBook,{bookType:"xlsx",type:"binary"})
      XLSX.writeFile(worKBook,"tableClient.xlsx")
    }
    
  return (
    <div className="App">
      {
         <MaterialTable
         title="tableau "
         columns={[
           { title: 'Code', field: 'Code_client' },
           { title: 'Web', field: 'Web' },
           { title: 'tel', field: 'Tel' },
         ]}
         data={this.state.client}//ta3ayet lstate mete3ik 

        //  localization={{ body: { emptyDataSourceMessage: 'noRecordToDisplay' }, header: { actions:'actions' }, pagination: { nextTooltip: 'nextpage', previousTooltip:'previouspage', lastTooltip: 'astpage', firstTooltip: 'firstpage', labelRowsSelect: 'rows' }, toolbar: { searchPlaceholder: 'search' } }}
         actions={[
          {
            icon : ()=><AddCircleOutlineIcon fontSize='large'/>,
            tooltip: 'Add User',
            isFreeAction: true,
            onClick: (event) => alert("You want to add a new row")
          },
          ({
            icon:()=><button>Export</button>,
            tooltip: 'export to excel',
            isFreeAction: true,
            onClick: () => downloadExel() ,
          }),
           row=>({
            icon:()=><DeleteIcon fontSize='large'/>,
            tooltip: 'delete',
            isFreeAction: true,
            onClick: (event,row) =>alert('Hello')
          }),
          row=>({
            icon:()=><EditIcon fontSize='large'/>,
            tooltip: 'Edit',
            onClick: (event,row) =>alert('React')
          })
         ]}
         options={{
          actionsColumnIndex:-1,
          search:true,
          pageSize:3,
          pageSizeOptions:[5,10,20],
          }}

       />
      }
     
    </div>
    );
}
 
}


export default App ; 





























//fare9 bin let, var, const, es6 ,react, dom  fari9 bin  vertuelledom w relledom w react bchkoun ye5dem 